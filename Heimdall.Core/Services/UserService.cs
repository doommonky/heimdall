﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Cryptography;
using System.Security.Claims;
using System.Text;
using Heimdall.Core.Models;
using Heimdall.Core.Models.Interfaces;
using Heimdall.Core.Roles;
using Heimdall.Core.Services.Interfaces;
using Microsoft.IdentityModel.Tokens;

namespace Heimdall.Core.Services
{
    public class UserService : IUserService
    {
        private readonly IPasswordService _passwordService;
        private readonly Dictionary<string, User> _usersCache;

        public UserService(IPasswordService passwordService)
        {
            _passwordService = passwordService;
            _usersCache = new Dictionary<string, User>();
        }

        public bool UserExists(string userEmail)
        {
            if (_usersCache.ContainsKey(userEmail))
                return true;

            return false;
        }

        public IUser GetUser(string userEmail)
        {
            return _usersCache[userEmail];
        }

        public IUser AddUser(string username, string userEmail, string password, Role role = Role.BaseUser)
        {
            var salt = _passwordService.GenerateSalt();
            var hash = _passwordService.HashPassword(password, salt);

            var user =  new User
            {
                ID = GenerateItempotentUserId(userEmail),
                UserName = username, 
                UserEmail = userEmail,
                PasswordHash = hash,
                PasswordSalt = salt,
                Roles = new HashSet<Role> { role }
            };

            _usersCache.Add(user.UserEmail, user);

            return user;
        }

        public IUser GetIdentityToken(string useremail)
        {
            if (string.IsNullOrEmpty(useremail))
                return null;
        
            var user = GetUser(useremail);
            if (user == null)
                return null;

            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.UTF8.GetBytes("Definitely A Secret");
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Email, useremail),
                    new Claim(ClaimTypes.GivenName, user.UserName),
                }),
                Expires = DateTime.UtcNow.AddHours(10),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };

            var token = tokenHandler.CreateToken(tokenDescriptor);
            user.Token = tokenHandler.WriteToken(token);

            return user;
        }

        public IUser RemoveIdentityToken(string userEmail)
        {
            if (string.IsNullOrEmpty(userEmail))
                return null;

            var user = GetUser(userEmail);
            if (user == null)
                return null;

            if (!string.IsNullOrEmpty(user.Token))
                user.Token = null;

            return user;
        }

        public bool DeleteUser(string userEmail)
        {
            _usersCache.Remove(userEmail);
            return true;
        }

        public Guid GenerateItempotentUserId(string userEmail)
        {
            using (var cryptoProvider = new MD5CryptoServiceProvider())
            {
                var usernameHash = cryptoProvider.ComputeHash(Encoding.UTF8.GetBytes(userEmail));
                return new Guid(usernameHash);
            }
        }
    }
}