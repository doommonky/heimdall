﻿using Heimdall.Core.Models;

namespace Heimdall.Core.Services.Interfaces
{
    public interface IPasswordService
    {
        byte[] HashPassword(string password, byte[] salt);
        byte[] HashPassword(byte[] password, byte[] salt);
        bool ComparePassword(string password, byte[] hash, byte[] salt);
        byte[] GenerateSalt();
    }
}
