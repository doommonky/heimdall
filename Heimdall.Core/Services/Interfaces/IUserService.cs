﻿using System;
using Heimdall.Core.Models.Interfaces;
using Heimdall.Core.Roles;

namespace Heimdall.Core.Services.Interfaces
{
    public interface IUserService
    {
        bool UserExists(string userEmail);
        IUser GetUser(string userEmail);
        IUser AddUser(string username, string userEmail, string password, Role role = Role.BaseUser);
        IUser GetIdentityToken(string useremail);
        IUser RemoveIdentityToken(string useremail);
        bool DeleteUser(string userEmail);
        Guid GenerateItempotentUserId(string userEmail);
    }
}