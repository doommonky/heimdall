﻿using System.Linq;
using System.Security.Cryptography;
using System.Text;
using Heimdall.Core.Services.Interfaces;

namespace Heimdall.Core.Services
{
    public class PasswordService : IPasswordService
    {
        public byte[] HashPassword(string password, byte[] salt)
        {
            var passwordBytes = Encoding.UTF8.GetBytes(password);
            return HashPassword(passwordBytes, salt);
        }

        public byte[] HashPassword(byte[] password, byte[] salt)
        {
            var saltedValue = password.Concat(salt).ToArray();
            return new SHA256Managed().ComputeHash(saltedValue);
        }

        public bool ComparePassword(string password, byte[] hash, byte[] salt)
        {
            var passwordHash = HashPassword(password, salt);

            return passwordHash.SequenceEqual(hash);
        }

        public byte[] GenerateSalt()
        {
            var rng = new RNGCryptoServiceProvider();
            byte[] salt = new byte[32];
            rng.GetNonZeroBytes(salt);

            return salt;
        }
    }
}
