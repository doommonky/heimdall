﻿using System;
using System.Collections.Generic;
using Heimdall.Core.Models.Interfaces;
using Heimdall.Core.Roles;

namespace Heimdall.Core.Models
{
    public class User : IUser
    {
        public Guid ID { get; set; }
        public string UserName { get; set; }
        public string UserEmail { get; set; }
        public byte[] PasswordHash { get; set; }
        public byte[] PasswordSalt { get; set; }
        public HashSet<Role> Roles { get; set; }
        public string Token { get; set; }
    }
}
