﻿using System;
using System.Collections.Generic;
using Heimdall.Core.Roles;

namespace Heimdall.Core.Models.Interfaces
{
    public interface IUser
    {
        Guid ID { get; set; }
        string UserName { get; set; }
        string UserEmail { get; set; }
        byte[] PasswordHash { get; set; }
        byte[] PasswordSalt { get; set; }
        HashSet<Role> Roles { get; set; }
        string Token { get; set; }
    }
}