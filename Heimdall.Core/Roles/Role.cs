﻿namespace Heimdall.Core.Roles
{
    public enum Role
    {
        BaseUser,
        SystemAdministrator,
        Administrator
    }
}