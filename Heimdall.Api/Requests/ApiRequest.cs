﻿using Newtonsoft.Json.Linq;

namespace Heimdall.Api.Requests
{
    public class ApiRequest
    {
        public JObject Body { get; set; }
    }
}
