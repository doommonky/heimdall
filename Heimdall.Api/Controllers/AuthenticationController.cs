﻿using Heimdall.Api.Requests;
using Heimdall.Core.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Heimdall.Api.Controllers
{
    [Route("api/[controller]")]
    public class AuthenticationController : ControllerBase
    {
        private readonly IPasswordService _passwordService;
        private readonly IUserService _userService;

        public AuthenticationController(IUserService userService, IPasswordService passwordService)
        {
            _passwordService = passwordService;
            _userService = userService;
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("login")]
        public IActionResult Login([FromBody] ApiRequest loginRequest)
        {
            if (loginRequest?.Body == null)
                return new BadRequestObjectResult("Login request could not be resolved");

            var userEmail = loginRequest.Body?["UserEmail"].ToString();
            var password = loginRequest.Body?["Password"].ToString();
            if (!_userService.UserExists(userEmail))
                return new NotFoundObjectResult("Requested user was not found.");

            var user = _userService.GetUser(userEmail);
            var passwordValid = _passwordService.ComparePassword(password, user.PasswordHash, user.PasswordSalt);

            if (!passwordValid)
                return new UnauthorizedObjectResult($"{userEmail} authentication failed.");
            else if (user.Token != null)
                return new OkObjectResult(user.Token);
            else
                _userService.GetIdentityToken(userEmail);

            return new OkObjectResult(user.Token);
        }

        [HttpPost]
        [Route("logout")]
        public IActionResult Logout([FromBody] ApiRequest logoutRequest)
        {
            var userEmail = logoutRequest?.Body?["UserEmail"].ToString();
            _userService.RemoveIdentityToken(userEmail);

            return new OkObjectResult($"{userEmail} successfully logged out.");
        }
    }
}