﻿using Heimdall.Api.Requests;
using Heimdall.Core.Models;
using Heimdall.Core.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;

namespace Heimdall.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;
        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        // GET api/user?userEmail={value}
        // [HttpGet]
        public ActionResult<User> Get(string userEmail)
        {
            if (!_userService.UserExists(userEmail))
                return new NotFoundObjectResult("Requested user not found.");

            var user = JObject.FromObject(_userService.GetUser(userEmail));
            user.Remove("PasswordHash");
            user.Remove("PasswordSalt");
            return new OkObjectResult(JObject.FromObject(user));
        }

        // POST api/user
        [HttpPost]
        public ActionResult Post([FromBody] ApiRequest userPost)
        {
            if (userPost == null)
                return new BadRequestObjectResult("Unable to resolve post body");

            var postBody = new 
            {
                UserEmail = (string)userPost?.Body?["UserEmail"],
                UserName = (string)userPost?.Body?["UserName"],
                Password = (string)userPost?.Body?["Password"]
            };

            if (postBody.UserEmail == null 
                || postBody.UserName == null
                || postBody.Password == null)
                return new BadRequestObjectResult("Unable to resolve post body");

            if (_userService.UserExists(postBody.UserEmail))
                return new BadRequestObjectResult($"Email {postBody.UserEmail} already has a user account associated with it.");

            var user = _userService.AddUser(postBody.UserName, postBody.UserEmail, postBody.Password);
            return new OkObjectResult($"User {user.UserName} has been successfully created.");
        }

        // DELETE api/user?userEmail={value}
        [HttpDelete]
        public ActionResult Delete(string userEmail)
        {
            if (!_userService.UserExists(userEmail))
                return new NotFoundObjectResult("Requested user not found.");

            _userService.DeleteUser(userEmail);
            return new OkObjectResult("Requested user was deleted.");
        }
    }
}
