using System;
using System.Text;
using Heimdall.Core.Models;
using Heimdall.Core.Services;
using Heimdall.Core.Services.Interfaces;
using NUnit.Framework;

namespace Heimdall.Core.Test.ServiceTests
{
    [TestFixture]
    public class PasswordServiceTests
    {
        private IPasswordService _passwordService;
        [SetUp]
        public void Setup()
        {
            _passwordService = new PasswordService();
        }

        [Test]
        public void GenerateSalt_SaltGenerated()
        {
            var salt = _passwordService.GenerateSalt();

            Assert.IsNotNull(salt);
            Assert.IsInstanceOf<byte[]>(salt);
        }

        [Test]
        public void ComparePassword_SaltingAndHashingResultsInPositiveComparison()
        {
            var password = "password";
            var salt = _passwordService.GenerateSalt();
            var hash = _passwordService.HashPassword(password, salt);

            Assert.IsTrue(_passwordService.ComparePassword("password", hash, salt));
        }

        [Test]
        public void ComparePassword_SaltingAndHashingDifferentPasswords_ResultsInNegativeComparison()
        {
            var password = "password";
            var salt = _passwordService.GenerateSalt();
            var hash = _passwordService.HashPassword(password, salt);

            Assert.IsFalse(_passwordService.ComparePassword("passphrase", hash, salt));
        }
    }
}