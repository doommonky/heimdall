using System;
using Heimdall.Core.Services;
using Heimdall.Core.Services.Interfaces;
using NUnit.Framework;

namespace Heimdall.Core.Test.ServiceTests
{ 
    [TestFixture]
    public class UserServiceTests 
    {
        private IUserService _userService;
        [SetUp]
        public void Setup()
        {
            _userService = new UserService(new PasswordService());
        }

        [Test]
        public void AddUser_UserAdded()
        {
            _userService.AddUser("John Doe", "jdoe@email.com", "password");
            var user = _userService.GetUser("jdoe@email.com");

            Assert.IsNotNull(user);
            Assert.AreNotEqual(Guid.Empty, user.ID);
            Assert.AreEqual("John Doe", user.UserName);
            Assert.AreEqual("jdoe@email.com", user.UserEmail);
            Assert.IsNotNull(user.PasswordSalt);
            Assert.IsNotNull(user.PasswordHash);
            Assert.IsInstanceOf<byte[]>(user.PasswordHash);
        }

        [Test]
        public void UserExists_AddUserAndCheckForExtantUser_UserCheckReturnsTrue()
        {
            _userService.AddUser("John Doe", "jdoe@email.com", "password");
            Assert.IsTrue(_userService.UserExists("jdoe@email.com"));
        }

        [Test]
        public void UserExists_CheckForExtantUser_UserCheckReturnsFalse()
        { 
            Assert.IsFalse(_userService.UserExists("jdoe@email.com"));
        }

        [Test]
        public void DeleteUser_UserDeleted()
        {
            _userService.AddUser("John Doe", "jdoe@email.com", "password");
            Assert.IsTrue(_userService.UserExists("jdoe@email.com"));
            _userService.DeleteUser("jdoe@email.com");
            Assert.IsFalse(_userService.UserExists("jdoe@gmail.com"));
        }

        [Test]
        public void GenerateIdempotentUserId_TwoRunsSameEmail_ReturnsSameGuid()
        {
            var userIdAttempt1 = _userService.GenerateItempotentUserId("jdoe@email.com");
            var userIdAttempt2 = _userService.GenerateItempotentUserId("jdoe@email.com");

            Assert.AreEqual(userIdAttempt1, userIdAttempt2);
        }

        [Test]
        public void GetIdentityToken_GeneratesValidJWTToken()
        {
            _userService.AddUser("John Doe", "jdoe@email.com", "password");
            var user = _userService.GetIdentityToken("jdoe@email.com");

            Assert.IsNotNull(user.Token);
        }
    }
}