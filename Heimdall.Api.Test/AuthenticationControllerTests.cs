using System;
using System.IdentityModel.Tokens.Jwt;
using System.Net.Http;
using System.Threading.Tasks;
using Heimdall.Api.Controllers;
using Heimdall.Api.Requests;
using Heimdall.Core.Models.Interfaces;
using Heimdall.Core.Services;
using Heimdall.Core.Services.Interfaces;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using NUnit.Framework;

namespace Heimdall.Api.Test
{
    public class AuthenticationControllerTests
    {
        private AuthenticationController _authenticationController;
        private UserService _userService;
        private PasswordService _passwordService;

            [SetUp]
        public void Setup()
        {
            _passwordService = new PasswordService();
            _userService = new UserService(_passwordService);
            _authenticationController = new AuthenticationController(_userService, _passwordService);
        }

        [Test]
        public async Task Login()
        {
            var user = _userService.AddUser("John Doe", "jdoe@email.com", "password");
            var request = new ApiRequest
            {
                Body = new JObject
                {
                    ["UserEmail"] = "jdoe@email.com",
                    ["Password"] = "password"
                }
            };
            var response = (OkObjectResult)_authenticationController.Login(request);
            var token = response.Value.ToString();
            var jsonTokenHandler = new JwtSecurityTokenHandler();
            var readToken = jsonTokenHandler.ReadToken(token);

            Assert.IsNotNull(token);
            //Assert.AreEqual("given_name: John Doe", readToken.);
        }
    }
}